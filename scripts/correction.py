#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 16:03:54 2023

@author: chevremo
"""
import numpy as np
from scipy.signal import oaconvolve
import matplotlib.pyplot as plt

from utils.h5_utils import read_rawfile
from utils.mask import getmask_edf
from utils.azim import azim, r2q


def correction(data, cx, cy, sub_d, window_data):
    
    # Get the subset of data
    subdata = np.array(data[int(cy)-sub_d:int(cy)+sub_d, int(cx)-sub_d:int(cx)+sub_d], dtype=np.float64)

    # Perform the convolution
    convd = oaconvolve(window_data, subdata, 'same')
    
    return convd

# Some variable definition: Select the data file and window
sample =        "Silica_600nm"        # Sample name
#sample =        "PMMA_800nm"          # Sample name
#sample =        "PS_2um"              # Sample name
d =             120                   # Distance used for the data subset

# Load the background, window and sample normalized and caved files
data_win, center_win, headers_win = read_rawfile('../data/window_norm_cave.h5')
data_bkg, center_bkg, headers_bkg = read_rawfile('../data/background_norm_cave.h5')
data,     center,     headers     = read_rawfile(f'../data/{sample}_norm_cave.h5')

# Load the data mask
mask = getmask_edf('../data/mask-31m_eiger2_frame1.edf')

# Perform the correction on data and background
data_conv = correction(data, center['cx'], center['cy'], d, data_win)
data_bkg_conv = correction(data_bkg, center_bkg['cx'], center_bkg['cy'], d, data_win)

# Compute the azimutal reduction for window, background and sample
r_win, I_win = azim(data_win, center_win['cx'], center_win['cy'])
# The distance should be the one of sample. This is actually not the window to detector distance, but this give the q-scale at which it appears on the sample measurement.
q_win = r2q(r_win, headers_win['lambdaw'], headers_win['pix_size'], headers['distance']) 

r_bkg, I_bkg = azim(data_bkg, center['cx'], center['cy'], mask=mask)
q_bkg = r2q(r_bkg, headers_bkg['lambdaw'], headers_bkg['pix_size'], headers_bkg['distance'])

r, I = azim(data, center['cx'], center['cy'], mask=mask)
q = r2q(r, headers['lambdaw'], headers['pix_size'], headers['distance'])


# Do the azimutal reduction for the secondary scattering convolution as well. The q_range and center are the one from the window in this case.
_, I_bkg_conv = azim(data_bkg_conv, center_win['cx'], center_win['cy'])
_, I_conv = azim(data_conv, center_win['cx'], center_win['cy'])

# Apply the correction
I_cor =         I[:(len(q_win))] - I_conv
I_bkg_cor =     I_bkg[:(len(q_win))] - I_bkg_conv

# Do the background subtraction
I_sub =         I - I_bkg
I_cor_sub =     I_cor - I_bkg_cor

# Display!

fig, ax = plt.subplots()

ax.plot(q_win, I_win*1e9, label="Window scattering (*1e9)")

ax.plot(q_bkg, I_bkg, label="Background uncorrected")
ax.plot(q_win, I_bkg_conv, label="Background correction")
ax.plot(q_win, I_bkg_cor, label="Background corrected")

ax.plot(q, I, label=f"{sample} uncorrected")
ax.plot(q_win, I_conv, label=f"{sample} correction")
ax.plot(q_win, I_cor, label=f"{sample} corrected")

ax.plot(q, I_sub, label=f"{sample} subtracted uncorrected")
ax.plot(q_win, I_cor_sub, label=f"{sample} subtracted corrected")

ax.set_xscale('log')
ax.set_yscale('log')
ax.legend()





