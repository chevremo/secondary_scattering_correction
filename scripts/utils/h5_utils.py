#!/usr/bin/env python3

# These functions are provided here as an example and should not be used in production environment.
# There is no garantee that these functions will work for other purpose than running the scripts provided here.
#
# (c) 2024 William Chevremont <william.chevremont@esrf.fr>
#
# When using the code provided in this repository, please cite <paper link and ref>
#
# This file contains helper functions to read/write H5 files.


import h5py, hdf5plugin
import numpy as np

from .nexus import getEntry, getDefaultData, get_headers

def read_rawfile(fn):
    """
    Read an HDF5 NeXuS file and return the data and useful headers
    
    @param fn: The file name
    
    @return data: The default data
    @return center: a cx, cy center dict
    @return headers: A dict containg useful metadata
    """
    
    with h5py.File(fn, 'r') as fd:
        entry = getEntry(fd)
        
        data = np.array(getDefaultData(fd, entry)['data'])
    
        H, _ = get_headers(fd, entry, "(Center_[1-2]|SampleDistance|WaveLength|PSize_1)")
        cx = float(H['Center_1'])
        cy = float(H['Center_2'])
        sdd = float(H['SampleDistance'])
        lambdaw = float(H['WaveLength'])
        pix_s = float(H['PSize_1'])
            
        return data[0], {'cx': cx, 'cy': cy}, {'distance': sdd, 'lambdaw': lambdaw, 'pix_size':pix_s}