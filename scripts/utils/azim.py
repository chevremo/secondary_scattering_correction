#!/usr/bin/env python3

# These functions are provided here as an example and should not be used in production environment.
# There is no garantee that these functions will work for other purpose than running the scripts provided here.
#
# (c) 2024 William Chevremont <william.chevremont@esrf.fr>
#
# When using the code provided in this repository, please cite <paper link and ref>
#
# This file contains helper functions to perform azimutal reduction using the histogram algorithm.

import numpy as np

def azim(data, cx, cy, mask=None):
    """
    Perform azimutal regrouping using the histogram algorithm. This assume the detector is perpendicular to the beam and pixels are square.
    
    @param data: The 2D image to reduce
    @param cx: Center in the x direction (2nd index in matrix)
    @param cy: Center in the y direction (1st index in matrix)
    @param mask: The mask to apply on data. If None, no mask are returned.
    
    @return r, I: The distance in pixel and intensity.
    """
    
    # Compute the distance of each pixel to the center
    Y,X = np.indices(data.shape)
    X = X - cx
    Y = Y - cy
    
    d = np.float32(np.sqrt(X**2+Y**2))    
    
    
    # Apply mask if any. Pixels with negative value are masked automatically.
    msk = data >= 0

    if mask is not None:
        np.logical_and(msk, mask, out=msk)
    
    saxs = data[msk]
    dh = d[msk]
    
    # rasterize the pixel distance
    dhi = np.array(dh, dtype=np.int32)
    
    # compute the average intensity on each bin
    ring_brightness = np.bincount(dhi, weights=saxs)
    rings = np.bincount(dhi)
    
    with np.errstate(divide='ignore',invalid='ignore'):
        I = ring_brightness/rings
        
    # Build the pixel distance vector
    r = np.arange(np.amax(dhi)+1)
    
    return r, I


def r2q(r, lambdaw, pix_size, distance):
    """
    Convert pixel distance to q in [unit]^-1. This assume the detector is perpendicular to the beam and pixel are square.
    
    @param r: The pixel distance to the center
    @param lambdaw: The wavelength in [unit]
    @param pix_size: The pixel size in [unit]
    @param distance: The sample to detector distance in [unit]
    
    @return q: The q vector in [unit]^-1
    """
    
    q = 4*np.pi/lambdaw*np.sin(np.arctan(pix_size*r/distance)/2.0)
    return q

