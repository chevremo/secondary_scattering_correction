#!/usr/bin/env python3

# These functions are provided here as an example and should not be used in production environment.
# There is no garantee that these functions will work for other purpose than running the scripts provided here.
#
# (c) 2024 William Chevremont <william.chevremont@esrf.fr>
#
# When using the code provided in this repository, please cite <paper link and ref>
#
# This file contains helper functions to read NeXuS files.

import re

def followlink(file, ds):
    """
    Return the physical path of a dataset
    
    @param file: The HDF5 file object
    @param ds: The logical dataset path
    
    @return The physical dataset path
    """
    
    return file[file[ds].ref].name


def to_str(k):
    """
    Convert anything to a string
    
    @param k: The anything to be converted
    
    @return: The anithing converted to string
    """
    
    if isinstance(k, str):
        return k
    elif isinstance(k, bytes):
        return k.decode()
    else:
        return str(k)


def checkClass(grp, e, klass):
    """
    Check that the HDF entry is a valid NeXuS class of klass
    
    @param grp: The group to be checked
    @param e: The specific entry to be checked
    @param klass: The NeXuS class to be checked
    
    @rtype: bool
    """
    
    gg = grp
    if e is not None:
        gg = grp[e]
    
    return 'NX_class' in gg.attrs and to_str(gg.attrs['NX_class']) == klass


def classIter(grp, klass):
    """
    List the available entries in grp corresponding to klass
    
    @param grp: The HDF group to iter in
    @param klass: The class object to be iterated
    
    @return: The entry path in group
    """
    
    for e in grp:
        if checkClass(grp, e, klass):
            yield e

def getClass(grp, klass):
    """
    List the available entries corresponding to klass in grp
    
    @param grp: The HDF group to iter in
    @param klass: The class object to be iterated
    
    @return: A list of HDF groups of class klass
    """
    r = []
    for e in classIter(grp, klass):
        r += [ grp[e], ]
            
    return r
    
#*
def getDetectors(file, entry):
    """
    Get the detector group of a file generated by LImA
    
    @param file: The file to look at
    @param entry: The entry to look at
    
    @return: The first NXdetector object
    """
    
    E = file[entry]
    
    for i in getClass(E, 'NXinstrument'):
        for d in getClass(i, 'NXdetector'):
            return d
            
    return None

def getPyFAI(file, entry):
    """
    Get the detector group of a file generated by PyFAI
    
    @param file: The file to look at
    @param entry: The entry to look at
    
    @return: The first NXdetector object
    """
    
    E = file[entry]
    
    for i in getClass(E, 'NXprocess'):
#        print(i.name)
        for d in getClass(i, 'NXcollection'):
#            print(d.name)
            if d.name.endswith('parameters'):
                return d
            
    return None

#*
def getEntry(file, entry=None):
    """
    Get the entries in the file. Return the first entry if entry is not specified
    
    @param file: The HDF file object
    @param entry: The entry name to look for
    
    @return: The entry object
    """
    
    entries = [ e for e in classIter(file['/'], 'NXentry') ]
            
    if entry is None:
        if len(entries) > 1:
            raise ValueError("Multiple entries available on file. Must select one.")
        else:
            entry = entries[0]
    elif entry not in entries:
        raise ValueError("Entry {entry} do not exists.")
            
    return entry

#*
def getDefaultData(file, entry=None):
    """
    Get the default data in a file. If entry is specified, take the default data of specific entry
    
    @param file: The HDF file object
    @param entry: The entry name to look for
    
    @return: The data object
    
    """
    
    entry = getEntry(file, entry)
    return _getDefaultData(file[entry])        
  
#*      
def _getDefaultData(grp):
    """
    Recursive look for default data
    """
    
    if checkClass(grp, None,'NXdata'):
        r = {}
        
        if 'signal' in grp.attrs:
            r['data'] = grp[to_str(grp.attrs['signal'])]
            
        if 'axes' in grp.attrs:
            r['axes'] = { 'axes': grp.attrs['axes']}
            for a in grp.attrs['axes']:
                dsnam = to_str(a)
                if dsnam != '.':
                    if dsnam in grp:
                        r['axes'][dsnam] = grp[dsnam]
            
        return r
    
    elif 'default' in grp.attrs:
        newpath = to_str(grp.attrs['default'])
        return _getDefaultData(grp[newpath])
    else:
        raise ValueError(f"Cannot find default data")
    
#*
def get_headers(file, entry, pattern = None):
    """
    Get the file headers
    
    @param file: The HDF file object
    @param entry: The entry to look at
    @param pattern: If defined, filter the headers according to this regex
    
    @return a dict of headers
    """
    
    detgrp = getDetectors(file, entry)
    
    if detgrp is not None:
        
        r = {}
        
        if 'header' in detgrp:
            for k in detgrp['header']:
                if pattern is None or re.search(pattern, k):
                    r[k] = detgrp['header'][k][()].decode()
        return r, detgrp['header']
                    
    else:
        pyfaiparams = getPyFAI(file, entry)
        
        r = {}
        for k in pyfaiparams:
            if pattern is None or re.search(pattern, k):
                r[k] = to_str(pyfaiparams[k][()])
                
        return r, pyfaiparams
        
        
    return r, None

