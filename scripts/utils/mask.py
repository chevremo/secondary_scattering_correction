#!/usr/bin/env python3

# These functions are provided here as an example and should not be used in production environment.
# There is no garantee that these functions will work for other purpose than running the scripts provided here.
#
# (c) 2024 William Chevremont <william.chevremont@esrf.fr>
#
# When using the code provided in this repository, please cite <paper link and ref>
#
# This file contains helper functions to read an EDF mask.

import fabio
import os
import numpy as np

def getmask_edf(filename):
    
    if not os.path.isfile(filename):
        return None, None
    
    with fabio.open(filename) as f:
        data = f.data
        
        return data >= 0



