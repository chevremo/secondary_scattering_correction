# Secondary Scattering Correction

This repository is an up-to-date version of the supporting information for article 
    
    Chèvremont, W. & Narayanan, T. (2024). A correction procedure for secondary scattering contributions from windows in small-angle X-ray scattering and ultra-small-angle X-ray scattering. J. Appl. Cryst. 57, 440-445.

https://onlinelibrary.wiley.com/iucr/doi/10.1107/S1600576724001997

Please, cite the above article when using this code.
